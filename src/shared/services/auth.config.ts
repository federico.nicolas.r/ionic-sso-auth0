export const AUTH_CONFIG = {
    // Needed for Auth0 (capitalization: ID):
    clientID: '0Qa_w9v3zFAq-ACEHv7BGtUOm2g8bRku',
    // Needed for Auth0Cordova (capitalization: Id):
    clientId: '0Qa_w9v3zFAq-ACEHv7BGtUOm2g8bRku',
    domain: 'osde-poc.auth0.com',
    packageIdentifier: 'io.ionic.starter' // config.xml widget ID, e.g., com.auth0.ionic
  };